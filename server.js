var express = require("express");
var app = express();
var exphbs = require('express-handlebars');
var bodyParser = require("body-parser");
var fs = require('fs');
var path = require ('path');
var homeRoutes = require('./routes/home');
var signRoutes = require('./routes/signin');
var resetRoutes = require('./routes/resetPass.js');
var logger = require('morgan');

var hbs = exphbs.create({
  defaultLayout: 'main',
  extname: 'hbs'
});

app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');
// app.use(logger('dev'));
app.use('/', homeRoutes);
app.use('/signin', signRoutes);
app.use('/resetPass', resetRoutes);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('public'));
app.use(express.urlencoded({extended: true}));

var PORT = process.env.PORT || 3000;

app.listen(PORT, function(){
    console.log('Server running on port ' + PORT);
});
