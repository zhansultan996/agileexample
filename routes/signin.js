var {Router} = require('express');
var router = Router();

router.get('/', function(req, res){
    res.render('signin', {
        title: 'Регистрация'
    });
});


module.exports = router;