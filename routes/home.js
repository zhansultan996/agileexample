var {Router} = require('express');
var router = Router();

router.get('/', function(req, res){
    res.render('index', {
        title: 'Главная страница'
    });
});


module.exports = router;
